package com.codecamp.simpleapi.service;

import java.util.List;

import com.codecamp.simpleapi.domain.User;
import com.codecamp.simpleapi.repository.UserRepository;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Iterable<User> list() {
        return userRepository.findAll();
    }

    public User getById(Long id) {
        return userRepository.findOne(id);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void save(List<User> users) {
        userRepository.save(users);
    }
}
